Silicon Beach Treatment Center

Silicon Beach Treatment Center is a substance abuse and mental health treatment center in Los Angeles, California that provides: partial hospitalization (PHP), intensive outpatient (IOP) and outpatient (OP) services for substance abuse and co-occurring disorders.

Address: 8929 S Sepulveda Blvd, Suite 200, Los Angeles, CA 90045, USA

Phone: 833-527-3422

Website: https://siliconbeachtx.com
